"{{{ Global initialization
if exists('g:ebuf_loaded')
  finish
endif
let g:ebuf_loaded = 1
"}}}
"{{{ s:get_visual_selection
func! s:get_visual_selection(type) abort
  let l:start_mark = "<"
  let l:end_mark = ">"
  if a:type ==# "char"
    let l:start_mark = "["
    let l:end_mark = "]"
  endif
  let [l:start_line, l:start_column] = getpos("'" .. l:start_mark)[1:2]
  let [l:end_line, l:end_column] = getpos("'" .. l:end_mark)[1:2]
  let l:lines = getline(l:start_line, l:end_line)
  if len(l:lines) == 0
    return ""
  endif
  if a:type ==# "\<c-v"
    " ctrl+V block
    let l:min_column = min([l:start_column, l:end_column])
    let l:max_column = max([l:start_column, l:end_column])
    for i in range(len(l:lines))
      let l:lines[i] = l:lines[i][l:min_column - 1 : l:max_column - 1]
    endfor
  elseif a:type ==# "line"
    let l:lines = [ getline(".") ]
  else
    let l:lines[-1] = l:lines[-1][: l:end_column - 1]
    let l:lines[0] = l:lines[0][l:start_column - 1 :]
  endif
  return join(l:lines, "\n")
endf
"}}}
"{{{ ebuf#set_xclip
func! ebuf#set_xclip(str) abort
  let l:job = job_start(['xclip', '-selection', 'clipboard'])
  let l:chn = job_getchannel(l:job)
  call ch_sendraw(l:chn, a:str)
  call ch_close_in(l:chn)
endf
"}}}
"{{{ ebuf#get_xclip
func! ebuf#get_xclip() abort
  return system('xclip -selection clipboard -o')
endf
"}}}
"{{{ ebuf#set_clipboard
func! ebuf#set_clipboard(str) abort
  let @@ = a:str
endf
"}}}
"{{{ ebuf#get_clipboard
func! ebuf#get_clipboard() abort
  return @@
endf
"}}}
"{{{ ebuf#copy_to_xclip
func! ebuf#copy_to_xclip(type) abort
  let l:text = s:get_visual_selection(a:type)
  call ebuf#set_xclip(l:text)
endf
"}}}
"{{{ ebuf#xclip_to_clipboard
func! ebuf#xclip_to_clipboard() abort
  call ebuf#set_clipboard(ebuf#get_xclip())
endf
"}}}
"{{{ ebuf#clipboard_to_xclip
func! ebuf#clipboard_to_xclip() abort
  call ebuf#set_xclip(ebuf#get_clipboard())
endf
"}}}
"{{{ ebuf#paste_from_xclip
func! ebuf#paste_from_xclip(type = v:true) abort
  call ebuf#xclip_to_clipboard()
  if a:type
    execute ":normal! p"
  else
    execute ":normal! P"
  endif
endf
"}}}
